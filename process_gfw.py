# Keiko Nomura and Sam Bowers, November 2018

# Import modules
import csv
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
from osgeo import gdal, gdalnumeric, ogr, osr
import pdb
from PIL import Image, ImageDraw
from scipy.ndimage.measurements import label
import scipy.ndimage
import shapefile
import datetime

# Define functions

def coordinateTransformer(shp):
    """
    Generates function to transform coordinates from a source shapefile CRS to Hansen data projection.
    
    Args:
        shp: Path to a shapefile.
    
    Returns:
        A function that transforms shapefile points to EPSG.
    """
    
    driver = ogr.GetDriverByName('ESRI Shapefile')
    ds = driver.Open(shp)
    layer = ds.GetLayer()
    spatialRef = layer.GetSpatialRef()
    
    # Create coordinate transformation
    inSpatialRef = osr.SpatialReference()
    inSpatialRef.ImportFromWkt(spatialRef.ExportToWkt())

    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(4326)

    coordTransform = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)
    
    return coordTransform


def world2Pixel(geo_t, x, y):
    """
    Uses a gdal geomatrix (ds.GetGeoTransform()) to calculate the pixel location of a geospatial coordinate.
    Modified from: http://geospatialpython.com/2011/02/clip-raster-using-shapefile.html.
    
    Args:
        geo_t: A gdal geoMatrix (ds.GetGeoTransform().
        x: x coordinate in map units.
        y: y coordinate in map units.
    
    Returns:
        A tuple with pixel/line locations for each input coordinate.
    """
    ulX = geo_t[0]
    ulY = geo_t[3]
    xDist = geo_t[1]
    yDist = geo_t[5]
    
    pixel = int((x - ulX) / xDist)
    line = int((y - ulY) / yDist)
    
    return (pixel, line)


def getTiles(data_dir, shp, data_type, version = '1.4', verbose = False):
    '''
    Takes a list of source files as input, and determines where each falls within extent of output tile.
    
    Args:
        infiles: A list of GeoTiff files from GFW
        shp: A shapefile containing AOI
        data_type: GFW data type, one of 'treecover', 'lossyear', or 'gain'
        version: GFW data version, defaults to '1.4'
        verbose: Set True to make script talkative
        
    Returns:
        A reduced list of scenes containing only files that will contribute to each country
    ''' 
    
    assert data_type in ['treecover', 'lossyear', 'gain'], "data_type must be 'treecover', 'lossyear', or 'gain'."
    
    if data_type == 'treecover': data_type += '2000'
    
    # Get list hansen data files
    infiles = sorted(glob.glob(data_dir + 'Hansen_GFC-2016-v%s_%s_*.tif'%(str(version), data_type)))
    
    # The shapefile may not have the same CRS as Hansen data, so this will generate a function to reproject points.
    coordTransform = coordinateTransformer(shp)
    
    # Read shapefile, and get bounding box in Hansen map coordinates
    sf = shapefile.Reader(shp) 
    sxmin, symin, sxmax, symax = sf.bbox
    sxmin, symin, _ = coordTransform.TransformPoint(sxmin, symin)
    sxmiax, symax, _ = coordTransform.TransformPoint(sxmax, symax)
    
    infiles_include = []
    for infile in infiles:
        
        ds = gdal.Open(infile,0)
        geo_t = ds.GetGeoTransform()
        
        # Skip this tile if it's out of bounds
        if sxmax < geo_t[0]: continue
        if sxmin > geo_t[0] + (geo_t[1] * ds.RasterYSize): continue
        if symax < geo_t[3] + (geo_t[5] * ds.RasterXSize): continue
        if symin > geo_t[3]: continue
    
        infiles_include.append(infile)
    
    return infiles_include


def getPixelsInCountry(treecover_files, AOI_shapefile, verbose = False):
    '''
    Calculates the total number of pixels in a shapefile.
    
    Args:
        treecover_files: List of GFW treecover files
        AOI_shapefile: Shapefile with area to include
        verbose: Make script talkative
    
    Returns:
        The number of pixels in a shapefile
    '''
    
    if verbose: print 'Counting country area in pixels for each tile...'
    
    pixels_country = 0
    
    for treecover_file in treecover_files:
        
        if verbose: print '    %s'%treecover_file.split('/')[-1]
        
        # Open GeoTiff file
        ds_treecover = gdal.Open(treecover_file)
        
        # Mask country, and add to pixel total
        pixels_country += rasterizeShapefile(ds_treecover, AOI_shapefile).sum()
    
    return pixels_country


def getCountryParameters(treecover_files, AOI_shapefile, country_area, min_area_ha, min_change_area_ha, verbose = False):
    '''
    Extract parameters for country, based on pixel area.
    
    Args:
        treecover_files: List of GFW treecover files
        AOI_shapefile: Shapefile delimiting country
        country_area: Total area of country (in km2)
        min_area_ha: Minimum forest area (in ha)
        min_change_area_ha: Minimum forest change area (in ha)
    Returns:
        Area of pixel in hectares, minumum forest area in pixels, minimum change area in pixels
    '''
    
    # Count pixels in country
    pixels_country = getPixelsInCountry(treecover_files, AOI_shapefile, verbose = verbose)
    
    # Convert hectares of min area & change area to pixels
    pixel_area = (float(country_area) * 1000 * 1000) / float(pixels_country) / 10000. # ha/pixel
    min_area = int(round(min_area_ha * (1./pixel_area)))
    min_change_area = int(round(min_change_area_ha * (1./pixel_area)))
    
    return pixel_area, min_area, min_change_area



def rasterizeShapefile(data, shp):
    """
    Rasterize points, lines or polygons from a shapefile to match a Hansen data tile
        
    Args:
        data: A Hansen data tile opened in gdal (gdal.Open(hansen_tile.tif')).
        shp: Path to a shapefile consisting of polygons. This does not have to be in the same projection as the dataset.
        buffer_size: Optionally specify a buffer to add around features of the shapefile, in decimal degrees.

    Returns:
        A numpy array with a boolean mask delineating locations inside (True) and outside (False) the shapefile [and optional buffer].
    """
    
    # Get geo transfrom
    geo_t = data.GetGeoTransform()
    
    # Create output image
    rasterPoly = Image.new("L", (data.RasterYSize, data.RasterXSize))
    rasterize = ImageDraw.Draw(rasterPoly)
    
    # The shapefile may not have the same CRS as Hansen data, so this will generate a function to reproject points.
    coordTransform = coordinateTransformer(shp)
    
    # Read shapefile
    sf = shapefile.Reader(shp) 
    
    # For each shape in shapefile...
    for shape in sf.iterShapes():
        
        # Get shape bounding box
        sxmin, symin, sxmax, symax = shape.bbox
        
        # Skip this tile if it's out of bounds
        if sxmax < geo_t[0]: continue
        if sxmin > geo_t[0] + (geo_t[1] * data.RasterYSize): continue
        if symax < geo_t[3] + (geo_t[5] * data.RasterXSize): continue
        if symin > geo_t[3]: continue
        
        #Separate polygons with list indices
        n_parts = len(shape.parts) #Number of parts
        indices = shape.parts #Get indices of shapefile part starts
        indices.append(len(shape.points)) #Add index of final vertex
        
        for part in range(n_parts):
            
            start_index = shape.parts[part]
            end_index = shape.parts[part+1]
            
            points = shape.points[start_index:end_index] #Map coordinates
            pixels = [] #Pixel coordinantes
            
            # Get bounding box of part
            pxmax = max([x[0] for x in points])
            pxmin = min([x[0] for x in points])
            pymax = max([y[1] for y in points])
            pymin = min([y[1] for y in points])
            
            # Skip this part if it's out of bounds
            if pxmax < geo_t[0]: continue
            if pxmin > geo_t[0] + (geo_t[1] * data.RasterYSize): continue
            if pymax < geo_t[3] + (geo_t[5] * data.RasterXSize): continue
            if pymin > geo_t[3]: continue
            
            # Transform coordinates to pixel values
            for p in points:
                
                # First update points from shapefile projection to ALOS mosaic projection
                lon, lat, z = coordTransform.TransformPoint(p[0], p[1])

                # Then convert map to pixel coordinates using geo transform
                pixels.append(world2Pixel(geo_t, lon, lat))

            # Draw the mask
            rasterize.polygon(pixels, 1)
            
    #Converts a Python Imaging Library array to a gdalnumeric image.
    mask = gdalnumeric.fromstring(rasterPoly.tobytes(),'b')
    mask.shape = rasterPoly.im.size[1], rasterPoly.im.size[0]
    
    return mask


def getContiguousAreas(data, min_val, max_val, min_area):
    '''
    Get pixels that come from the same contigous area.
    
    Args:
        data: A numpy array of Hansen data
        min_val: The minimum value to contain within the cotiguous area (inclusive)
        max_val: The maxiumum value to contain within the contigous area (inclusive)
        min_area: What minimum area should be included (number of pixels, queens move)
    
    Returns:
        A binary array of pixels that meet the conditiuons
    '''
    
    # Extract area that meets min_val and max_val conditions
    binary_array = np.logical_and(data >= min_val, data <= max_val) * 1
    
    # Label contigous forest areas with a number
    s = scipy.ndimage.generate_binary_structure(2,2) # This connects diagonal elements
    location_id, n_areas = label(binary_array, structure = s)
    
    # Get count of each value in array
    label_area = np.bincount(location_id.flatten())[1:]
    
    # Find those IDs that meet minimum area requirements
    include_id = np.arange(1, n_areas + 1)[label_area >= min_area]
    
    # Create empty output array
    contiguous_area = np.zeros_like(data, dtype=np.bool)
    
    step = 1000
    # Fill it in a bit at a time
    for xmin in np.arange(0,40000,step):
        for ymin in np.arange(0,40000,step):
                xmax = xmin + step
                ymax = ymin + step
                contiguous_area[ymin:ymax,xmin:xmax] = np.in1d(location_id[ymin:ymax,xmin:xmax], include_id).reshape((step,step))
    
    # Get a binary array of forest_id pixels that belong to the minimum area requirement
    #contiguous_area = np.in1d(location_id, include_id).reshape(data.shape) * 1
    
    return contiguous_area




if __name__ == '__main__':
    
    '''
    This script:
        1) Loops through GFW data treecover/lossyear/gain files
        2) Determines which pixels meet national forest density/area requirements
        3) Calculates area of forest in each tile
        4) Calculates rate of forest loss in each tile, based on national forest definitions
        5) Calculates area of forest gain in each tile, based on national forest definitions
    '''
    
    ################ 
    ### Settings ###
    ################

    # Definition of tree cover for this country (Cambodia as example)
    country_name = 'Cambodia'
    country_area = 182612 # square km
    min_treecover_def = 10 #percent, inclusive
    min_area_ha = 0.5 #hectares (for forest cover)
    min_change_area_ha = 5 #hectares (for loss and gain)
    
    # Hansen data version
    version = '1.4'
    
    # Input data locations
    data_directory = '%s/DATA/%s/'%(os.path.dirname(os.path.realpath(__file__)), country_name)
    AOI_shapefile = '%s/DATA/%s/KHM_adm0.shp'%(os.path.dirname(os.path.realpath(__file__)), country_name)
    
    # Print progress
    verbose = True

    ################
    ### The Code ###
    ################
    
    # Get list hansen data files
    treecover_files = getTiles(data_directory, AOI_shapefile, 'treecover', version = version)
    lossyear_files = getTiles(data_directory, AOI_shapefile, 'lossyear', version = version)
    gain_files = getTiles(data_directory, AOI_shapefile, 'gain', version = version)
    
    # Check that all required files are present
    assert len(lossyear_files) == len(treecover_files), 'There must be one treecover_file for each lossyear_file in %s'%data_directory
        
    # Calculate number of pixels that make up min forest area and min change areas
    pixel_area, min_area, min_change_area = getCountryParameters(treecover_files, AOI_shapefile, country_area, min_area_ha, min_change_area_ha, verbose = verbose)

    # Add 1 to min treecover (which must be at least 1 % greater than min value)
    min_treecover = min_treecover_def + 1
    
    # For each input tile
    for treecover_file, lossyear_file, gain_file in zip (treecover_files, lossyear_files, gain_files):
        
        if verbose: print 'Doing %s'%treecover_file
        
        # Get latitude/longitude details from file name
        lat = treecover_file.split('/')[-1].split('_')[-2]
        lon = treecover_file.split('/')[-1].split('_')[-1].split('.')[0]

        # Open GeoTiff files
        ds_treecover = gdal.Open(treecover_file)
        ds_lossyear = gdal.Open(lossyear_file)
        ds_gain = gdal.Open(gain_file)
        
        # Mask out areas outside of shapefile
        if verbose: print '    masking out country area...'
        country_mask = rasterizeShapefile(ds_treecover, AOI_shapefile)
        
        
        ###TREECOVER##

        # Load tree cover dataset
        treecover = ds_treecover.ReadAsArray()
        
        # Get pixels meeting canopy cover definition, and mask areas outside country
        if verbose: print '    calculating pixels that meet canopy cover requirement...'
        forest_area_2000 = treecover >= min_treecover 
        forest_area_2000[country_mask == False] = False
          
        # Calculate pixels that meet conditons of minimum area to be counted as forest
        if verbose: print '    calculating pixels that meet forest conditions...'
        forest_area_2000_size = getContiguousAreas(forest_area_2000 * 1, 1, 1, min_area)
        
        
        ###LOSS###
        
        if verbose: print '    calculating loss pixels from deforestation and fragmentation...'
        
        # Load loss year dataset
        lossyear = ds_lossyear.ReadAsArray()
        
        # A loss can only occur in the country and if the pixel first met the forest condition
        lossyear[np.logical_or(country_mask == False, forest_area_2000 == False)] = 0
                
        # Determination of the forest remaining at the end of the measurement period.
        
        # Get the forest area at the end of the time period
        forest_area_2017 = forest_area_2000.copy()
        forest_area_2017[np.logical_and(lossyear > 0, lossyear<17)] = False
        
        # Apply minumum forest area condition
        forest_area_2017_size = getContiguousAreas(forest_area_2017*1,1,1,min_area)
                
        # True forest loss (including fragment losses)   
        loss_area = np.logical_and(forest_area_2000_size, forest_area_2017_size==False)
        
        # Calculate those losses that meet the minimum change area requirement
        loss_area_size = getContiguousAreas(loss_area * 1, 1, 1, min_change_area) == 1
        
        # Free memory
        forest_area_2017 = None
        forest_area_2017_size = None
        loss_area = None
        
        # Segment losses into deforestation and fragment types
        loss_area_deforestation_size = np.logical_and(loss_area_size,np.logical_and(lossyear >= 1, lossyear < 17))
        loss_area_fragment_size = np.logical_and(loss_area_size, lossyear == 0)
        
        # Initialise counter to track which pixels have been already counted as loss
        forest_loss_tracker = np.zeros_like(forest_area_2000_size, dtype=np.bool)
        
        pixels_loss = []
        
        if verbose: print '    calculating loss rate for year:'       
        for year in range(2001,2017):
            
            if verbose: print '       ...%s'%str(year)
                    
            # Get loss pixels for a given year from deforestation
            loss_pixels_deforestation = np.logical_and(lossyear == (year - 2000), loss_area_deforestation_size)
            
            # Calculate loss pixels to date
            loss_pixels_to_date = np.logical_and(np.logical_and(lossyear > 0, lossyear <= (year - 2000)), loss_area_size)
            
            # Get remaining forest
            remaining_forest = np.logical_and(forest_area_2000_size, loss_pixels_to_date == False)
            remaining_forest_size = getContiguousAreas(remaining_forest*1,1,1,min_area) == 1
            
            # Get loss pixels for a given year from fragmentation
            loss_pixels_fragment = np.logical_and(remaining_forest_size==False, loss_area_fragment_size)
                    
            # Take away fragment pixels that have aleady been counted
            loss_pixels_fragment[forest_loss_tracker] = 0

            # Append them to an output list
            pixels_loss.append(loss_pixels_deforestation.sum() + loss_pixels_fragment.sum())
            
            # Update the pixels that have already been counted as loss
            forest_loss_tracker[np.logical_or(loss_pixels_deforestation,loss_pixels_fragment)] = True
            
            if year==2012:
                # Save this in order to figure out gains (2000-2012 only)
                loss_2012 = forest_loss_tracker.copy()
        
        # Free memory
        loss_area_natural = None
        loss_pixels_fragment = None
        loss_area_deforestation_size = None
        loss_area_fragment_size = None
        forest_loss_tracker = None
        loss_pixels_to_date = None
        
        
        ###GAIN###
        
        # Load gain file
        if verbose: print '    calculating gain pixels...'
        gain = ds_gain.ReadAsArray() == 1 
        
        # Apply country mask
        gain[country_mask==False] = False
        
        # Calculate area of forest in 2012 (from forest area in 2000, loss, and gain)
        forest_area_2012 = forest_area_2000.copy()
        forest_area_2012[loss_2012] = False
        forest_area_2012[gain] = True
        
        # Apply min forest area requirement
        forest_area_2012_size = getContiguousAreas(forest_area_2012*1,1,1,min_area)
        
        # Free memory
        forest_area_2012 = None
        
        # Forest gain can either occur where forest never existed...
        gain_area_clean_2012 = np.logical_and(forest_area_2000_size==False,forest_area_2012_size==True) * 1
        
        # ... or where deforested and subsequently gained
        gain_area_regrowth_2012 = np.logical_and(np.logical_and(forest_area_2000_size==True,forest_area_2012_size==True), np.logical_and(gain,loss_2012))
        
        # ... or gains occur over areas that were forest in 2000, and were never deforested. Either GFW missed the loss that occured in the intermediate period, or forest cover in 2000 was overestimated
        gain_area_phantom_2012 = np.logical_and(np.logical_and(forest_area_2000_size==True,forest_area_2012_size==True), np.logical_and(gain,loss_2012==False)) # Not included
        
        # Locate the pixels that meet the minimum change area requirement
        gain_area_2012_size = getContiguousAreas(np.logical_or(gain_area_clean_2012,gain_area_regrowth_2012)*1,1,1,min_change_area) == 1
        
        pixels_gain = gain_area_2012_size.sum()
        
        # Output results for tile in csv
        with open('%s_%s_%s.csv'%(country_name, lat, lon), 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter = ',')
            writer.writerow(['Description', 'Value'])
            writer.writerow(['Pixels in country mask', str(country_mask.sum())])
            writer.writerow(['Pixels of forest in 2000', str(forest_area_2000_size.sum())])
            for n,year in enumerate(range(2001,2017)):
                writer.writerow(['Loss in %s'%str(year), str(pixels_loss[n])])
            writer.writerow(['Gain', str(pixels_gain)])
            writer.writerow(['Pixel area (ha)', str(pixel_area)])
            writer.writerow(['Number of connected pixels for forest', str(min_area)])
            writer.writerow(['Number of connected pixels for change', str(min_change_area)])
            writer.writerow(['Country Area', str(country_area)])
            writer.writerow(['Canopy Cover', str(min_treecover_def)])
            writer.writerow(['Min Forest Area', str(min_area_ha)])
            writer.writerow(['Min Change Area', str(min_change_area_ha)])
