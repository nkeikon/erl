## Missed carbon emissions from forests: comparing countries' estimates submitted to UNFCCC to biophysical estimates

Written in Python 2.7 by Keiko Nomura (keiko.nomura@ed.ac.uk) and Samuel Bowers (sam.bowers@ed.ac.uk).

Requires common python libraries, plus gdal, pyshp, and PIL, all available as part of Anaconda Python.

Article available at: http://iopscience.iop.org/article/10.1088/1748-9326/aafc6b
